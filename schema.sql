
DROP TABLE IF EXISTS events CASCADE;
DROP TABLE IF EXISTS users CASCADE;

CREATE TABLE users(
   id               			SERIAL PRIMARY KEY,
   username          			VARCHAR(30) NOT NULL,
   email                    	VARCHAR(40) NOT NULL,
   password            		    VARCHAR(30) NOT NULL                   
);

CREATE TABLE events(
   id               			SERIAL PRIMARY KEY,
   user_id          			INTEGER REFERENCES users (id),
   type                    		VARCHAR(20) NOT NULL,
   event_time            		VARCHAR(30) NOT NULL,                    
   location              		VARCHAR(50),
   title            			TEXT NOT NULL,
   notes             			TEXT
);


