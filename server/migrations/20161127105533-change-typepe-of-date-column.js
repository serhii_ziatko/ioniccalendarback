'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    queryInterface.addColumn(
      'Events',
      'date',
      {
        type: Sequelize.DATE,
        allowNull: false
      }
    )
  },

  down: function (queryInterface, Sequelize) {
   queryInterface.removeColumn('Events', 'date')

  }
};
