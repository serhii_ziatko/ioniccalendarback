'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    queryInterface.addColumn(
      'Events',
      'endDate',
      {
        type: Sequelize.DATE
      }
    )
  },

  down: function (queryInterface, Sequelize) {
   
  }
};
