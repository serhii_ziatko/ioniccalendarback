module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    username: {
      type: DataTypes.STRING,
      validate: {
        not: /^\d/,
        notEmpty: true,
        max: 10,           
        min: 2
      },
      unique: true
    },
    email: {
      type: DataTypes.STRING,
      validate: {
        isEmail: true, 
        notEmpty: true
      },
      unique: true
    },
    password:{
      type: DataTypes.STRING,
      validate: {
        notEmpty: true,
        min: 6
      }
    }
  }, {
    classMethods: {
      associate: function(models) {
         User.hasMany(models.Events);
      }
    }
  });
  return User;
};