const express = require('express');
const router = express.Router();
const Sequelize  = require('sequelize');
const models = require('../server/models/index');

function getPeriod(date){
  let first = new Date(date).setUTCDate(-10);
  first = new Date(first).toISOString()
  let last = new Date(date).setUTCDate(40);
  last = new Date(last).toISOString();
  //console.log(first, last);
  return {first, last};
}

// get events for three month
router.post('/', function(req, res, next) {  
  const period = getPeriod(req.body.date);
  models.Events.findAll({
    where: {
      date: {
        gt: period.first,
        lt: period.last
      },
      UserId: req.body.userId
    },
    order: "date"
  }).then(result => {
    res.status(201).json(result)
  }, err => {
    next(err);
  })
});

// remove single event
router.delete('/:id', function(req, res, next) {
  models.Events.destroy({
    where: {
      id: req.params.id
    }
  }).then(result => {
    res.json(result);
  }, err => {
    next(err);
  });
});

// update single todo
router.put('/:id', function(req, res, next) {
  models.Events.find({
    where: {
      id: req.params.id
    }
  }).then( event => {
    if(event){
      event.updateAttributes({
        type: req.body.type,
        date: req.body.date,
        endDate: req.body.endDate,
        title: req.body.title,
        notes: req.body.notes,
        location: req.body.location,
      }).then(result => {
        res.send(result);
      }, err => {
        next(err);
      });
    }
  });
});

// create new event
router.post('/new-event', function(req, res, next) { 
  return models.Events.create({
    type: req.body.type,
    date: req.body.date,
    endDate: req.body.endDate,
    title: req.body.title,
    notes: req.body.notes,
    location: req.body.location,
    UserId: req.body.UserId,
  }).then( result => {
      res.status(201).json(result)
    }, err => {
      next(err);
  });
   
});


// create new event
router.post('/all', function(req, res, next) { 
  return models.Events.findAll({
    where: {
      UserId: req.body.id
    }
  }).then( result => {
      res.status(201).json(result)
    }, err => {
      next(err);
  });
   
});

module.exports = router;
