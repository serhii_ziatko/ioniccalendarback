const express = require('express');
const router = express.Router();
const Sequelize  = require('sequelize');
const models = require('../server/models/index');

function sendEmail(){
  console.log('email was send');
};


// Sing Up
router.post('/singup', (req, res, next) => {
  models.User.create({
        username: req.body.username,
        email: req.body.email,
        password: req.body.password
  }).then( result => {
    res.status(201).json(result)
  }, err => {
    next(err);
  });
});

// Login
router.post('/login', (req, res, next) => {
  models.User.findOne({
    where: {
      username: req.body.username,
      password: req.body.password
    }
  }).then(user => {
      if ( !user ){
        next();
      }
      res.status(201).json({id: user.id, username: user.username, email: user.email});
    }, next);
});

// Restore password
router.post('/restorepassword', (req, res, next) => {
  models.User.findOne({
    where: {
      email: req.body.email
    }
  }).then(user => {
      if ( !user ) {
        next();
      }
      sendEmail();
      res.status(201).json({email: user.email});
   }, next);
});

module.exports = router;
